<?php

require __DIR__ . '/helper/generalRequires.php';
require __DIR__ . '/helper/requireLoginCheck.php';

if (!$loggedIn) {
    // User not logged in, redirect to login page with error message
    $errorMsg = 'Please log in to change your account. For security reasons, edited data has not been saved.';
    $_SESSION['errors'][] = $errorMsg;
    header('Location: ./login.php?error=settingsCallbackFail');
    die($errorMsg);
}

if (isset($_REQUEST['update'])) {
    switch ($_REQUEST['update']) {
        case 'password':
            $pdo->beginTransaction();
            try {
                
            } catch (PDOException $ex) {
                $pdo->rollBack();
                $_SESSION['errors'][] = 'Database access failed:<br />' . htmlentities($ex->getMessage());
            }
            break;
    }
} else {
    // There is no handler implemented for this request - cancel everything and go back to overview
    $errorMsg = 'The sent request could not be handled.';
    $_SESSION['errors'][] = $errorMsg;
    header('Location: .');
    die($errorMsg);
}