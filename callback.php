<?php

require __DIR__ . '/helper/generalRequires.php';
require __DIR__ . '/helper/requireLoginCheck.php';

if (!$loggedIn) {
    // User not logged in, redirect to login page with error message
    $errorMsg = 'Please log in to modify your saved credentials. For security reasons, edited data has not been saved.';
    $_SESSION['errors'][] = $errorMsg;
    header('Location: ./login.php?error=callbackFail');
    die($errorMsg);
}

if (isset($_REQUEST['add'])) {
    // Adding new credential
    $action = 'add';
    $credId = 0;
    $existNeeded = PWMGR_EXIST_NEEDED_NONE;
} else if (isset($_REQUEST['update'])) {
    // Editing existing credential
    $action = 'update';
    $credId = $_REQUEST['update'];
    $existNeeded = PWMGR_EXIST_NEEDED_CRED;
} else if (isset($_REQUEST['copy'])) {
    // Adding modified existing credential
    $action = 'add';
    $credId = $_REQUEST['copy'];
    $existNeeded = PWMGR_EXIST_NEEDED_CRED;
} else if (isset($_REQUEST['delete'])) {
    // Deleting a credential
    $action = 'delete';
    $credId = $_REQUEST['delete'];
    $existNeeded = PWMGR_EXIST_NEEDED_CRED;
} else if (isset($_REQUEST['rename-category'])) {
    // Renaming a category
    $action = 'rename-category';
    $categoryId = $_REQUEST['rename-category'];
    $existNeeded = PWMGR_EXIST_NEEDED_CAT;
} else if (isset($_REQUEST['category-up'])) {
    // Renaming a category
    $action = 'category-up';
    $categoryId = $_REQUEST['category-up'];
    $existNeeded = PWMGR_EXIST_NEEDED_CAT;
} else if (isset($_REQUEST['category-down'])) {
    // Renaming a category
    $action = 'category-down';
    $categoryId = $_REQUEST['category-down'];
    $existNeeded = PWMGR_EXIST_NEEDED_CAT;
} else if (isset($_REQUEST['cred-up'])) {
    // Renaming a category
    $action = 'cred-up';
    $credId = $_REQUEST['cred-up'];
    $existNeeded = PWMGR_EXIST_NEEDED_CRED;
} else if (isset($_REQUEST['cred-down'])) {
    // Renaming a category
    $action = 'cred-down';
    $credId = $_REQUEST['cred-down'];
    $existNeeded = PWMGR_EXIST_NEEDED_CRED;
} else {
    // There is no handler implemented for this request - cancel everything and go back to overview
    $errorMsg = 'The sent request could not be handled.';
    $_SESSION['errors'][] = $errorMsg;
    header('Location: .');
    die($errorMsg);
}

try {
    $pdo->beginTransaction();
    
    // Encryption key is binary MD5 Hash of user's password
    $enckey = md5($_SESSION['password'], true);
    
    if ($existNeeded & PWMGR_EXIST_NEEDED_CRED) {
        // The given credential needs to exist in the given user's account
        // Check if the credential to update exists and get its Encryption IV
        $sql = 'SELECT encryption_iv FROM mgrcredential WHERE id = :id AND userid = :uid';
        $stmt = $pdo->prepare($sql);
        $stmt->execute([':id' => $credId, ':uid' => $userId]);
        
        if ($stmt->rowCount() > 0) {
            // Credential exists, get its Encryption IV
            $enciv = hex2bin($stmt->fetchColumn());
        } else {
            // Credential does not exist, throw an error
            throw new InvalidArgumentException('The selected credential does not exist in your account.');
        }
    }
    if ($existNeeded & PWMGR_EXIST_NEEDED_CAT) {
        // The given category needs to exist in the given user's account
        // Check if the category to update exists and get its Encryption IV
        $sql = 'SELECT encryption_iv FROM mgrcategory WHERE id = :cid AND userid = :uid';
        $stmt = $pdo->prepare($sql);
        $stmt->execute([':cid' => $categoryId, ':uid' => $userId]);
        
        if ($stmt->rowCount() > 0) {
            // Category exists, get its Encryption IV
            $enciv = hex2bin($stmt->fetchColumn());
        } else {
            // Category does not exist, throw an error
            throw new InvalidArgumentException('The selected category does not exist in your account.');
        }
    }
    
    switch ($action) {
        case 'add':
            // Check the Submitted Category
            $categoryId = $_REQUEST['category'];
            require __DIR__ . '/helper/checkCategoryId.php';
            
            // Prepare Update SQL Statement
            $sql = 'INSERT INTO mgrcredential ' .
                '(userid, categoryid, bezeichnung, domain, username, password, encryption_iv, description, everythingEncrypted) ' .
                'VALUES (:uid, :cid, :bez, :dom, :un, :pw, :iv, :desc, 1) ';
            $stmt = $pdo->prepare($sql);
            
            // Generate Encryption IV
            $enciv = random_bytes(openssl_cipher_iv_length(PWMGR_ENC_METHOD));
            
            // Store everything encrypted except for the User ID and Encryption IV
            $stmt->execute([
                ':uid' => $userId,
                ':cid' => $categoryId,
                ':bez' => openssl_encrypt($_REQUEST['bezeichnung'], PWMGR_ENC_METHOD, $enckey, 0, $enciv),
                ':dom' => openssl_encrypt($_REQUEST['domain'], PWMGR_ENC_METHOD, $enckey, 0, $enciv),
                ':un' => openssl_encrypt($_REQUEST['username'], PWMGR_ENC_METHOD, $enckey, 0, $enciv),
                ':pw' => openssl_encrypt($_REQUEST['password'], PWMGR_ENC_METHOD, $enckey, 0, $enciv),
                ':desc' => openssl_encrypt($_REQUEST['description'], PWMGR_ENC_METHOD, $enckey, 0, $enciv),
                ':iv' => bin2hex($enciv)
            ]);
            
            // Get the ID of the added credential (i.e. the newest ID of this user)
            $stmt = $pdo->prepare('SELECT id FROM mgrcredential WHERE userid = :uid ORDER BY id DESC LIMIT 1');
            $stmt->execute([':uid' => $userId]);
            $credId = $stmt->fetchColumn();
            
            // Commit SQL transaction
            $pdo->commit();
            
            // Let the overview immediately scroll to the new credential
            header('Location: .?selected=' . $credId . '#credential-' . $credId);
            break;
            
        case 'update':
            // Check the Submitted Category
            $categoryId = $_REQUEST['category'];
            require __DIR__ . '/helper/checkCategoryId.php';
            
            // Prepare Update SQL Statement
            $sql = 'UPDATE mgrcredential ' .
                'SET categoryid = :cid, bezeichnung = :bez, domain = :dom, username = :un, password = :pw, description = :desc, everythingEncrypted = 1 ' .
                'WHERE id = :id AND userid = :uid';
            $stmt = $pdo->prepare($sql);
            
            // Store everything encrypted
            $stmt->execute([
                ':uid' => $userId,
                ':id' => $credId,
                ':cid' => $categoryId,
                ':bez' => openssl_encrypt($_REQUEST['bezeichnung'], PWMGR_ENC_METHOD, $enckey, 0, $enciv),
                ':dom' => openssl_encrypt($_REQUEST['domain'], PWMGR_ENC_METHOD, $enckey, 0, $enciv),
                ':un' => openssl_encrypt($_REQUEST['username'], PWMGR_ENC_METHOD, $enckey, 0, $enciv),
                ':pw' => openssl_encrypt($_REQUEST['password'], PWMGR_ENC_METHOD, $enckey, 0, $enciv),
                ':desc' => openssl_encrypt($_REQUEST['description'], PWMGR_ENC_METHOD, $enckey, 0, $enciv)
            ]);
            
            // Commit SQL transaction
            $pdo->commit();
            
            // Let the overview immediately scroll to the changed credential
            header('Location: .?selected=' . $credId . '#credential-' . $credId);
            break;
            
        case 'delete':
            $sql = 'DELETE FROM mgrcredential WHERE id = :id AND userid = :uid';
            $stmt = $pdo->prepare($sql);
            $stmt->execute([
                ':uid' => $userId,
                ':id' => $credId
            ]);
            
            // Commit SQL transaction
            $pdo->commit();
            
            // Let the overview display a success message
            header('Location: .?success=delete');
            break;
            
        case 'rename-category':
            $sql = 'UPDATE mgrcategory SET description = :desc WHERE id = :cid AND userid = :uid';
            $stmt = $pdo->prepare($sql);
            $stmt->execute([
                ':uid' => $userId,
                ':cid' => $categoryId,
                ':desc' => openssl_encrypt($_REQUEST['bezeichnung'], PWMGR_ENC_METHOD, $enckey, 0, $enciv)
            ]);
            
            // Commit SQL transaction
            $pdo->commit();
            
            // Let the overview open the changed category
            header('Location: .?selected-category=' . $categoryId . '#category-' . $categoryId);
            break;
            
        case 'category-up':
        case 'category-down':
            $sql = 'SELECT ROW_NUMBER() OVER (ORDER BY sort ASC, id ASC) AS rownum, id FROM mgrcategory ' .
                'WHERE userid = :uid ORDER BY rownum ASC';
            $stmt = $pdo->prepare($sql);
            $stmt->execute([':uid' => $userId]);
            $categories = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $sql = 'UPDATE mgrcategory SET sort = :srt WHERE id = :id AND userid = :uid';
            foreach ($categories as $rownum => $category) {
                $stmt = $pdo->prepare($sql);
                if (
                    // When moving a category up, the category above
                    // (i.e. the one where the category below [$rownum+1] has the moving ID)
                    // shall move down [$rownum++]
                    ($action == 'category-up' && $categories[$rownum+1]['id'] == $categoryId) ||
                    // When moving a category down, it shall move down [$rownum++]
                    ($action == 'category-down' && $category['id'] == $categoryId)
                ) {
                    $stmt->execute([
                        ':srt' => $rownum+1,
                        ':id' => $category['id'],
                        ':uid' => $userId
                    ]);
                } else if (
                    // When moving a category down, the category below
                    // (i.e. the one where the category above [$rownum-1] has the moving ID)
                    // shall move up [$rownum--]
                    ($action == 'category-down' && $categories[$rownum-1]['id'] == $categoryId) ||
                    // When moving a category up, it shall move up [$rownum--]
                    ($action == 'category-up' && $category['id'] == $categoryId)
                ) {
                    $stmt->execute([
                        ':srt' => $rownum-1,
                        ':id' => $category['id'],
                        ':uid' => $userId
                    ]);
                } else {
                    $stmt->execute([
                        ':srt' => $rownum,
                        ':id' => $category['id'],
                        ':uid' => $userId
                    ]);
                }
            }
            
            // Commit SQL transaction
            $pdo->commit();
            
            // Let the overview open the changed category
            header('Location: .?selected-category=' . $categoryId . '#category-' . $categoryId);
            break;
            
        case 'cred-up':
        case 'cred-down':
            $sql = 'SELECT categoryid FROM mgrcredential WHERE id = :id AND userid = :uid';
            $stmt = $pdo->prepare($sql);
            $stmt->execute([':uid' => $userId, ':id' => $credId]);
            $categoryId = $stmt->fetchColumn();
            
            if ($categoryId === null) {
                $sql = 'SELECT ROW_NUMBER() OVER (ORDER BY sort ASC, id ASC) AS rownum, id FROM mgrcredential ' .
                    'WHERE userid = :uid AND categoryid IS NULL ' .
                    'ORDER BY rownum ASC';
                $stmt = $pdo->prepare($sql);
                $stmt->execute([':uid' => $userId]);
            } else {
                $sql = 'SELECT ROW_NUMBER() OVER (ORDER BY sort ASC, id ASC) AS rownum, id FROM mgrcredential ' .
                    'WHERE userid = :uid AND categoryid = :catid ' .
                    'ORDER BY rownum ASC';
                $stmt = $pdo->prepare($sql);
                $stmt->execute([':uid' => $userId, ':catid' => $categoryId]);
            }
            $credentials = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $sql = 'UPDATE mgrcredential SET sort = :srt WHERE id = :id AND userid = :uid';
            foreach ($credentials as $rownum => $cred) {
                $stmt = $pdo->prepare($sql);
                if (
                    // When moving a credential up, the category above
                    // (i.e. the one where the credential below [$rownum+1] has the moving ID)
                    // shall move down [$rownum++]
                    ($action == 'cred-up' && $credentials[$rownum+1]['id'] == $credId) ||
                    // When moving a credential down, it shall move down [$rownum++]
                    ($action == 'cred-down' && $cred['id'] == $credId)
                    ) {
                        $stmt->execute([
                            ':srt' => $rownum+1,
                            ':id' => $cred['id'],
                            ':uid' => $userId
                        ]);
                    } else if (
                        // When moving a credential down, the category below
                        // (i.e. the one where the credential above [$rownum-1] has the moving ID)
                        // shall move up [$rownum--]
                        ($action == 'cred-down' && $credentials[$rownum-1]['id'] == $credId) ||
                        // When moving a credential up, it shall move up [$rownum--]
                        ($action == 'cred-up' && $cred['id'] == $credId)
                        ) {
                            $stmt->execute([
                                ':srt' => $rownum-1,
                                ':id' => $cred['id'],
                                ':uid' => $userId
                            ]);
                        } else {
                            $stmt->execute([
                                ':srt' => $rownum,
                                ':id' => $cred['id'],
                                ':uid' => $userId
                            ]);
                        }
            }
            
            // Commit SQL transaction
            $pdo->commit();
            
            // Let the overview immediately scroll to the changed credential
            header('Location: .?selected=' . $credId . '#credential-' . $credId);
            break;
            
            
            
        default:
            // There is no handler implemented for this action - cancel everything and go back to overview
            throw new InvalidArgumentException('The defined action has not yet been implemented.');
            break;
    }
} catch (PDOException $ex) {
    // If an error with the database occurs, safely undo everything and 
    $pdo->rollBack();
    $errorMsg = 'Database access failed:<br />' . htmlentities($ex->getMessage());
    $_SESSION['errors'][] = $errorMsg;
    header('Location: .');
    die($errorMsg);
} catch (InvalidArgumentException $ex) {
    $pdo->rollBack();
    $errorMsg = htmlentities($ex->getMessage());
    $_SESSION['errors'][] = $errorMsg;
    header('Location: .');
    die($errorMsg);
} catch (Exception $ex) {
    $pdo->rollBack();
    $errorMsg = htmlentities($ex->getMessage());
    $_SESSION['errors'][] = $errorMsg;
    header('Location: .');
    die($errorMsg);
}
