<?php
$userId = null;
$loggedIn = false;

// Check user data
if (isset($_SESSION['username']) && isset($_SESSION['password'])) {
    if (isset($pdo)) {
        $sql = 'SELECT id, masterpw, loginFails FROM mgruser WHERE username = :un OR email = :un';
        $stmt = $pdo->prepare($sql);
        $stmt->execute([':un' => $_SESSION['username']]);
        
        if ($stmt->rowCount() > 0) {
            // User exists, get their data
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
            $userId = $result['id'];
            
            if ($result['loginFails'] < 3) {
                // User is valid and active, check their password
                if (password_verify($_SESSION['password'], $result['masterpw'])) {
                    // Password OK, Login OK, reset failed login attempts to 0
                    $loggedIn = true;
                    $sql = 'UPDATE mgruser SET loginFails = 0 WHERE id = :id';
                    $stmt2 = $pdo->prepare($sql);
                    $stmt2->execute([':id' => $userId]);
                    
                    // DO NOT unset the password, it is needed in further use!
                } else {
                    // Wrong password, increment failed login attempts
                    $_SESSION['errors'][] = 'A user with this username/e-mail and password could not be found.';
                    $sql = 'UPDATE mgruser SET loginFails = loginFails + 1 WHERE id = :id';
                    $stmt2 = $pdo->prepare($sql);
                    $stmt2->execute([':id' => $userId]);
                    
                    // Unset the password to prohibit double checking and early account locking
                    unset($_SESSION['password']);
                }
            } else {
                // Too many failed login attempts, Account locked
                $_SESSION['errors'][] = 'Your account is locked. Please contact the team.';
                
                // Unset the password to prohibit double checking
                unset($_SESSION['password']);
            }
        } else {
            // User does not exist
            $_SESSION['errors'][] = 'A user with this username/e-mail and password could not be found.';
            
            // Unset the password to prohibit double checking
            unset($_SESSION['password']);
        }
    }
}
