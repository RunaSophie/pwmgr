<nav class="navbar">
  <a href="." class="navbar-element navbar-element-image<?php if ($navbarSelPage == 'overview') echo ' selected' ?>">
    <img src="img/logo-short-square.png" height="30px" />
    <span style="padding-left: 10px; padding-right: 5px;">Overview</span>
  </a>
  <?php
    if ($navbarSelPage == 'login') {
      ?>
      <a href="login.php" class="navbar-element selected">
        Log In
      </a>
      <?php
    } else {
      ?>
      <a href="logout.php" class="navbar-element">
        Log Out
      </a>
      <?php
    }
  ?>
  <a href="" class="navbar-element<?php if ($navbarSelPage == 'settings') echo ' selected' ?>">
    User Settings
  </a>
</nav>