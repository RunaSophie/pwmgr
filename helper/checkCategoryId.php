<?php
switch ($categoryId) {
    case 'null':
        $categoryId = null;
        break;
        
    case 'new':
        $sql = 'INSERT INTO mgrcategory (userid, description, encryption_iv) ' .
            'VALUES (:uid, :desc, :iv) ';
        $stmt = $pdo->prepare($sql);
        
        $cat_enciv = random_bytes(openssl_cipher_iv_length(PWMGR_ENC_METHOD));
        $description = openssl_encrypt('New Category', PWMGR_ENC_METHOD, $enckey, 0, $cat_enciv);
        
        $stmt->execute([
            ':uid' => $userId,
            ':desc' => $description,
            ':iv' => bin2hex($cat_enciv)
        ]);
        
        $sql = 'SELECT id FROM mgrcategory ORDER BY id DESC LIMIT 1 ';
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $categoryId = $stmt->fetchColumn();
        break;
        
    default:
        $sql = 'SELECT id FROM mgrcategory WHERE id = :cid AND userid = :uid ';
        $stmt = $pdo->prepare($sql);
        $stmt->execute([':cid' => $categoryId, ':uid' => $userId]);
        
        if ($stmt->rowCount() > 0) {
            $categoryId = $stmt->fetchColumn();
        } else {
            throw new InvalidArgumentException('Category ID is invalid!');
        }
        break;
}