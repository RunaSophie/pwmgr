<div class="accordion-item" id="credential-<?= $credential['id'] ?>">
  <input type="checkbox" id="cred-<?= $credential['id'] ?>-radio" class="accordion-item-radio" <?php if ($credential['checked']) echo 'checked' ?> />
  <?php if (strpos($credential['id'], 'new') === false) { ?>
    <label for="cred-<?= $credential['id'] ?>-radio" class="accordion-item-hidewhenopen credential-title">
      <div><?= $credential['bezeichnung'] ?></div>
      <div><?= $credential['domain'] ?></div>
    </label>
    <label for="cred-<?= $credential['id'] ?>-radio" class="accordion-item-content credential-title">
      Edit Credential
    </label>
  <?php } else { ?>
    <label for="cred-<?= $credential['id'] ?>-radio" class="credential-title">
      Add New Credential
    </label>
  <?php } ?>
  <form class="accordion-item-content" action="callback.php" method="POST">
    <div class="form-group">
      <div class="width-3 form-field">
        <label for="cred-<?= $credential['id'] ?>-cat" class="form-label">Category:</label>
      </div>
      <div class="width-6 form-field">
        <select id="cred-<?= $credential['id'] ?>-cat" class="form-input" name="category">
          <?php foreach ($categories as $cat_select) { ?>
            <option value="<?= $cat_select['id'] ?>"
                <?php if (isset($category) && isset($category['id']) && $cat_select['id'] == $category['id']) echo 'selected'; ?>>
              <?= $cat_select['description'] ?>
            </option>
          <?php } ?>
          <option value="new">New Category...</option>
        </select>
      </div>
      <?php if (strpos($credential['id'], 'new') === false) { ?>
        <div class="width-2 form-field" style="justify-content: flex-end">
          <?php
            if (!$credential['first']) {
              ?>
              <button class="button-edit button-with-image" type="submit" name="cred-up" value="<?= $credential['id'] ?>">
                <img style="transform: rotate(90deg)" src="img/arc.svg" alt="Open" height="20px" />
              </button>
              <?php
            } if (!$credential['last']) {
              ?>
              <button class="button-edit button-with-image" type="submit" name="cred-down" value="<?= $credential['id'] ?>">
                <img style="transform: rotate(-90deg)" src="img/arc.svg" alt="Open" height="20px" />
              </button>
              <?php
            }
          ?>
        </div>
      <?php } ?>
    </div>
    <div class="form-group">
      <div class="width-3 form-field">
        <label for="cred-<?= $credential['id'] ?>-bez" class="form-label">Name:</label>
      </div>
      <div class="width-6 form-field">
        <input id="cred-<?= $credential['id'] ?>-bez" class="form-input" type="text" name="bezeichnung" value="<?= $credential['bezeichnung'] ?>" />
      </div>
    </div>
    <div class="form-group">
      <div class="width-3 form-field">
        <label for="cred-<?= $credential['id'] ?>-domain" class="form-label">Location:</label>
      </div>
      <div class="width-6 form-field">
        <input id="cred-<?= $credential['id'] ?>-domain" class="form-input" type="text" name="domain" value="<?= $credential['domain'] ?>" />
        <button type="button" onclick="openLocation(&quot;cred-<?= $credential['id'] ?>-domain&quot;)" class="button-edit button-with-image javascript-only"><img style="transform: rotate(135deg)" src="img/arc.svg" alt="Open" height="20px" /></button>
      </div>
    </div>
    <div class="form-group">
      <div class="width-3 form-field">
        <label for="cred-<?= $credential['id'] ?>-username" class="form-label">Username:</label>
      </div>
      <div class="width-6 form-field">
        <input id="cred-<?= $credential['id'] ?>-username" class="form-input" type="text" name="username" value="<?= $credential['username'] ?>" />
        <button type="button" onclick="copyUsername(&quot;cred-<?= $credential['id'] ?>-username&quot;)" class="button-edit button-with-image javascript-only"><img src="img/copy.svg" alt="Copy Username" height="20px" /></button>
      </div>
    </div>
    <div class="form-group">
      <div class="width-3 form-field">
        <label for="cred-<?= $credential['id'] ?>-password" class="form-label">Password:</label>
      </div>
      <div class="width-6 form-field">
        <input id="cred-<?= $credential['id'] ?>-password" class="form-input" type="text" name="password" value="<?= $credential['password'] ?>" />
        <button type="button" onclick="copyPassword(&quot;cred-<?= $credential['id'] ?>-password&quot;)" class="button-edit button-with-image javascript-only"><img src="img/copy.svg" alt="Copy Password" height="20px" /></button>
      </div>
    </div>
    <div class="form-group">
      <div class="width-3 form-field">
        <label for="cred-<?= $credential['id'] ?>-desc" class="form-label">Notes:</label>
      </div>
      <div class="width-6 form-field">
        <textarea id="cred-<?= $credential['id'] ?>-desc" class="form-input" name="description"><?= $credential['description'] ?></textarea>
      </div>
    </div>
    <?php if (strpos($credential['id'], 'new') === false) { ?>
      <div class="form-group">
        <input type="radio" name="cred-<?= $credential['id'] ?>-buttons" id="cred-<?= $credential['id'] ?>-buttons-normal" class="accordion-item-radio" checked />
        <button class="button-submit accordion-item-content" type="submit" name="update" value="<?= $credential['id'] ?>">Save</button>
        <button class="button-edit accordion-item-content" type="submit" name="copy" value="<?= $credential['id'] ?>">Save as Copy</button>
        <label for="cred-<?= $credential['id'] ?>-buttons-delete" class="accordion-item-content button-delete">Delete</label>
        <label for="cred-<?= $credential['id'] ?>-radio" class="accordion-item-content button-edit">Cancel</label>
      </div>
      <div class="form-group">
        <input type="radio" name="cred-<?= $credential['id'] ?>-buttons" id="cred-<?= $credential['id'] ?>-buttons-delete" class="accordion-item-radio" />
        <label class="accordion-item-content width-12">Please confirm deletion as this action is irreversible:</label>
        <button class="button-delete accordion-item-content" type="submit" name="delete" value="<?= $credential['id'] ?>">Yes, Delete</button>
        <label for="cred-<?= $credential['id'] ?>-buttons-normal" class="accordion-item-content button-edit">No, Cancel</label>
      </div>
    <?php } else { ?>
      <div class="form-group">
        <button class="button-submit accordion-item-content" type="submit" name="add">Add</button>
        <label for="cred-<?= $credential['id'] ?>-radio" class="accordion-item-content button-edit">Cancel</label>
      </div>
    <?php } ?>
  </form>
</div>