<?php
// Display errors in a list if there are any
if ($_SESSION['errors'] != []) {
?>
  <ul class="error-list">
    <?php
      foreach ($_SESSION['errors'] as $error) {
        echo "<li>" . $error . "</li>\n";
      }
    ?>
  </ul>
  <?php
}
// Reset displayed errors so they don't appear another time
$_SESSION['errors'] = [];