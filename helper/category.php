<div class="accordion-item" id="category-<?= $category['id'] ?>">
  <input type="checkbox" id="category-<?= $category['id'] ?>-radio" class="accordion-item-radio" <?php if ($category['checked']) echo 'checked'; ?> />
  <label for="category-<?= $category['id'] ?>-radio" class="credential-title">
    <?= $category['description'] ?>
  </label>
  <div class="accordion-item-content">
    <?php
      // Display Rename Field except if it's the "Uncategorised" Category
      if (isset($category['id']) && $category['id'] != 'null') {
        ?>
        <form class="form-group" action="callback.php" method="POST">
          <div class="width-3 form-field">
            <label for="cat-<?= $category['id'] ?>-desc" class="form-label">Rename:</label>
          </div>
          <div class="width-5 form-field">
            <input id="cat-<?= $category['id'] ?>-desc" class="form-input" type="text" name="bezeichnung" value="<?= $category['description'] ?>" />
          </div>
          <div class="width-2 form-field">
            <button class="button-edit" type="submit" name="rename-category" value="<?= $category['id'] ?>">Rename!</button>
          </div>
          <div class="width-2 form-field" style="justify-content: flex-end">
            <?php
              if (!$category['first']) {
                ?>
                <button class="button-edit button-with-image" type="submit" name="category-up" value="<?= $category['id'] ?>">
                  <img style="transform: rotate(90deg)" src="img/arc.svg" alt="Open" height="20px" />
                </button>
                <?php
              } if (!$category['last']) {
                ?>
                <button class="button-edit button-with-image" type="submit" name="category-down" value="<?= $category['id'] ?>">
                  <img style="transform: rotate(-90deg)" src="img/arc.svg" alt="Open" height="20px" />
                </button>
                <?php
              }
            ?>
          </div>
        </form>
        <?php 
      }
      
      // Add empty credential at the beginning
      $credential = $credential_blank;
      $credential['id'] = $category['id'] . '-new';
      include __DIR__ . '/credential.php';

      // Display all credentials
      foreach ($category['credentials'] as $credential) {
        include __DIR__ . '/credential.php';
      }
    ?>
  </div>
</div>