<?php
session_name('pwmgr_v2');
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('PWMGR_ENC_METHOD', 'AES-256-CBC');
define('PWMGR_EXIST_NEEDED_NONE', 0);
define('PWMGR_EXIST_NEEDED_CRED', 1);
define('PWMGR_EXIST_NEEDED_CAT', 2);

if (!isset($_SESSION['errors'])) {
    $_SESSION['errors'] = [];
}
$pdo = null;
$userId = 0;

// Database Connection
try {
    $pdo = new PDO('mysql:host=localhost;dbname=pwmgr;port=3306', 'pwmgruser', ':$=\'>/$q>OGH*zjhf"H}');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $ex) {
    $_SESSION['errors'][] = 'Database connection failed:<br />' . htmlentities($ex->getMessage());
}
