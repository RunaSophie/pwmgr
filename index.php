<?php

require __DIR__ . '/helper/generalRequires.php';
require __DIR__ . '/helper/requireLoginCheck.php';

$navbarSelPage = 'overview';

if (!$loggedIn) {
    // User is not logged in, redirect to login page
    header('Location: ./login.php?redir=.');
    die('Redirect to Login Page');
}

// Encryption key is binary MD5 Hash of user's password. NEVER EVER print out or save anywhere!
$enckey = md5($_SESSION['password'], true);

// Select all credential categories of the logged in user
$sql = 'SELECT id, description AS desc_db, encryption_iv FROM mgrcategory ' .
       'WHERE userid = :uid ORDER BY sort ASC, id ASC';
$stmt = $pdo->prepare($sql);
$stmt->execute([':uid' => $userId]);
$categories = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Decrypt the category name and select all associated credentials
foreach ($categories as &$category) {
    $category['first'] = false;
    $category['last'] = false;
    
    $category['description'] = openssl_decrypt($category['desc_db'], PWMGR_ENC_METHOD, $enckey, 0, hex2bin($category['encryption_iv']));
    
    $sql = 'SELECT id, bezeichnung AS bez_db, domain AS dom_db, username AS un_db, password AS pw_db, ' .
                  'description AS desc_db, encryption_iv, everythingEncrypted ' .
           'FROM mgrcredential WHERE userid = :uid AND categoryid = :cat ORDER BY sort ASC, id ASC';
    $stmt = $pdo->prepare($sql);
    $stmt->execute([':uid' => $userId, ':cat' => $category['id']]);
    
    $category['credentials'] = $stmt->fetchAll(\PDO::FETCH_ASSOC);
}
$category['last'] = true;
$categories[0]['first'] = true;

// Select all uncategorised credentials
// Includes credentials associated with another user's category
$sql = 'SELECT id, bezeichnung AS bez_db, domain AS dom_db, username AS un_db, password AS pw_db, ' .
              'description AS desc_db, encryption_iv, everythingEncrypted ' .
       'FROM mgrcredential ' .
       'WHERE userid = :uid AND (categoryid IS NULL OR categoryid NOT IN (SELECT id FROM mgrcategory WHERE userid = :uid)) ' .
       'ORDER BY sort ASC, id ASC';
$stmt = $pdo->prepare($sql);
$stmt->execute([':uid' => $userId]);

// Add uncategorised credentials as a new category
$categories[] = [
    'id' => 'null',
    'description' => '--- Uncategorised ---',
    'credentials' => $stmt->fetchAll(PDO::FETCH_ASSOC)
];

// Define the opened credential
if (isset($_REQUEST['selected'])) {
    $selCred = $_REQUEST['selected'];
} else {
    $selCred = 0;
}

// Define the opened category
if (isset($_REQUEST['selected-category'])) {
    $selCat = $_REQUEST['selected-category'];
} else {
    $selCat = 0;
}

// Decrypt credentials
foreach ($categories as &$category) {
    $category['checked'] = ($selCat == $category['id']);
    
    foreach ($category['credentials'] as &$cred) {
        $cred['last'] = false;
        $cred['first'] = false;
        
        if ($selCred == $cred['id']) {
            $category['checked'] = true;
            $cred['checked'] = true;
        } else {
            $cred['checked'] = false;
        }
        
        $cred['password'] = openssl_decrypt($cred['pw_db'], PWMGR_ENC_METHOD, $enckey, 0, hex2bin($cred['encryption_iv']));
        
        if ($cred['everythingEncrypted']) {
            $cred['bezeichnung'] = openssl_decrypt($cred['bez_db'], PWMGR_ENC_METHOD, $enckey, 0, hex2bin($cred['encryption_iv']));
            $cred['domain'] = openssl_decrypt($cred['dom_db'], PWMGR_ENC_METHOD, $enckey, 0, hex2bin($cred['encryption_iv']));
            $cred['username'] = openssl_decrypt($cred['un_db'], PWMGR_ENC_METHOD, $enckey, 0, hex2bin($cred['encryption_iv']));
            $cred['description'] = openssl_decrypt($cred['desc_db'], PWMGR_ENC_METHOD, $enckey, 0, hex2bin($cred['encryption_iv']));
        } else {
            $cred['bezeichnung'] = $cred['bez_db'];
            $cred['domain'] = $cred['dom_db'];
            $cred['username'] = $cred['un_db'];
            $cred['description'] = $cred['desc_db'];
        }
    }
    $cred['last'] = true;
    $category['credentials'][0]['first'] = true;
    
    unset($cred);
}

unset($category);

// Create a blank credential
$credential_blank = [
    'id' => 'new',
    'bezeichnung' => '',
    'domain' => '',
    'username' => '',
    'password' => '',
    'description' => '',
    'checked' => false
];

?>
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <title>Overview | Focaccina PWMGR</title>
    <link rel="stylesheet" href="style.css" />
    <link rel="icon" type="image/x-icon" href="img/logo-short-square.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <?php include __DIR__ . '/helper/navbar.php';?>
    <div class="container">
      <h1>Overview</h1>
      <ul class="notify-list" style="visibility: collapse;" id="notifylist"></ul>
      <?php
        include __DIR__ . '/helper/errorList.php';
        $credential = $credential_blank;
        include __DIR__ . '/helper/credential.php'
      ?>
      <div class="accordion" id="categories">
        <?php
          foreach ($categories as &$category) {
              include __DIR__ . '/helper/category.php';
          }
        ?>
      </div>
    </div>
    <script>
      // Unset the Notification List Timer
      let timeoutNotifyList = null;

      // Display Javascript-Only Elements by removing the corresponding class
      Array.from(document.getElementsByClassName("javascript-only")).forEach((item) => { item.classList.remove("javascript-only"); });
      
      // Display success messages
      switch (<?= json_encode(isset($_REQUEST['success']) ? $_REQUEST['success'] : '') ?>) {
        case 'delete':
          document.getElementById("notifylist").innerHTML = "<li>Credential successfully deleted!<\/li>";
          setNotifyList();
          break;

        case 'rename-category':
          document.getElementById("notifylist").innerHTML = "<li>Category successfully renamed!<\/li>";
          setNotifyList();
          break;
      }
      
      // Open the content of the parsed field in a new tab
      // Displays a link-related success message
      function openLocation(elementId) {
        let location = document.getElementById(elementId).value;
        window.open('http://' + location, '_blank');
        document.getElementById("notifylist").innerHTML = "<li>Link opened!<\/li>";
        setNotifyList();
      }

      // Copy the content of the parsed field to clipboard
      // Displays a username-related success message
      function copyUsername(elementId) {
        let copiedText = document.getElementById(elementId).value;
        navigator.clipboard.writeText(copiedText);
        document.getElementById("notifylist").innerHTML = "<li>Username copied!<\/li>";
        setNotifyList();
      }
      

      // Copy the content of the parsed field to clipboard
      // Displays a password-related success message
      function copyPassword(elementId) {
        let copiedText = document.getElementById(elementId).value;
        navigator.clipboard.writeText(copiedText);
        document.getElementById("notifylist").innerHTML = "<li>Password copied!<\/li>";
        setNotifyList();
      }

      // Resets the timer of the notification list
      function setNotifyList() {
        clearTimeout(timeoutNotifyList);
        document.getElementById("notifylist").style.visibility = "visible";
        timeoutNotifyList = setTimeout(hideNotifyList, 10000);
      }

      // Hides the notification list, called when timer ends
      function hideNotifyList() {
        document.getElementById("notifylist").style.visibility = "collapse";
        document.getElementById("notifylist").innerHTML = "";
      }
    </script>
  </body>
</html>