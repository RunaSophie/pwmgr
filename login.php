<?php

require __DIR__ . '/helper/generalRequires.php';
require __DIR__ . '/helper/requireLoginCheck.php';

$navbarSelPage = 'login';

// Prepare the redirection
if (isset($_REQUEST['redir'])) {
    $redir = './' . $_REQUEST['redir'];
} else {
    $redir = '.';
}

if ($loggedIn) {
    // User is already logged in, redirect to destination
    header('Location: ' . $redir);
} else {
    // User is not logged in
    // Re-build the GET arguments
    $getArgs = [];
    
    if (isset($_REQUEST['redir'])) {
        $getArgs['redir'] = $_REQUEST['redir'];
        $_SESSION['errors'][] = 'Please log in to access your credentials.';
    }
    
    if (isset($_REQUEST['error'])) {
        $getArgs['errors'] = $_REQUEST['error'];
    }
    
    // Look for a given username
    $username = '';
    
    if (isset($_SESSION['username'])) {
        $username = $_SESSION['username'];
    }
    
    if (isset($_REQUEST['username'])) {
        $username = $_REQUEST['username'];
    }
}
?>
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <title>Login | Focaccina PWMGR</title>
    <link rel="stylesheet" href="style.css" />
    <link rel="icon" type="image/x-icon" href="img/logo-short-square.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <?php include __DIR__ . '/helper/navbar.php';?>
    <div class="container">
      <h1>Login</h1>
      <ul class="notify-list" style="visibility: collapse;" id="notifylist"></ul>
      <?php include __DIR__ . '/helper/errorList.php' ?>
      <form action="loginCheck.php<?php if (!empty($getArgs)) { echo '?' . http_build_query($getArgs); } ?>"
            method="POST">
        <div class="form-group">
          <label for="username" class="form-label width-2 width-12-sm">Username:</label><br />
          <input type="text" class="form-input width-4 width-12-sm" id="username" name="username" value="<?= $username ?>" />
        </div>
        <div class="form-group">
          <label for="password" class="form-label width-2 width-12-sm">Password:</label><br />
          <input type="password" class="form-input width-4 width-12-sm" id="password" name="password" />
        </div>
        <div class="form-group">
          <button type="submit" class="button-submit" name="login">Log in</button>
        </div>
      </form>
    </div>
  </body>
</html>