<?php

require __DIR__ . '/helper/generalRequires.php';
require __DIR__ . '/helper/requireLoginCheck.php';

$navbarSelPage = 'settings';

if (!$loggedIn) {
    // User is not logged in, redirect to login page
    header('Location: ./login.php?redir=settings.php');
    die('Redirect to Login Page');
}

?>
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <title>User Settings | Focaccina PWMGR</title>
    <link rel="stylesheet" href="style.css" />
    <link rel="icon" type="image/x-icon" href="img/logo-short-square.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <?php include __DIR__ . '/helper/navbar.php';?>
    <div class="container">
      <h1>User Settings</h1>
      <ul class="notify-list" style="visibility: collapse;" id="notifylist"></ul>
      <?php include __DIR__ . '/helper/errorList.php' ?>
      <h2>Change Password</h2>
      <form action="settings-callback.php" method="POST">
        <div class="form-group">
          <label for="currPassword" class="form-label width-3 width-12-sm">Current Password:</label><br />
          <input type="password" class="form-input width-6 width-12-sm" id="currPassword" name="currPassword" />
        </div>
        <div class="form-group">
          <label for="newPassword" class="form-label width-3 width-12-sm">New Password:</label><br />
          <input type="password" class="form-input width-6 width-12-sm" id="newPassword" name="newPassword" />
        </div>
        <div class="form-group">
          <label for="repPassword" class="form-label width-3 width-12-sm">Repeat New Password:</label><br />
          <input type="password" class="form-input width-6 width-12-sm" id="repPassword" name="repPassword" />
        </div>
        <div class="form-group">
          <button class="button-submit" type="submit" name="update" value="password">Save</button>
        </div>
      </form>
    </div>
  </body>
</html>