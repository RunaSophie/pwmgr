<?php

require __DIR__ . '/helper/generalRequires.php';

// Save Login Data into Session
if (isset($_POST['login'])) {
    if (isset($_POST['username'])) {
        $_SESSION['username'] = $_REQUEST['username'];
    }
    if (isset($_POST['password'])) {
        $_SESSION['password'] = $_REQUEST['password'];
    }
}

require __DIR__ . '/helper/requireLoginCheck.php';

// Prepare the redirection
if (isset($_REQUEST['redir'])) {
    $redir = './' . $_REQUEST['redir'];
} else {
    $redir = '.';
}

if ($loggedIn) {
    // User successfully logged in, redirect to destination without errors
    $_SESSION['errors'] = [];
    if (isset($_REQUEST['error'])) {
        switch ($_REQUEST['error']) {
            case 'callbackFail':
                // If there was a callback fail, tell the user to re-initiate the requested action.
                $_SESSION['errors'][] = 'The requested action could not be completed due to authentication errors. Please retry.';
                break;
        }
    }
    header('Location: ' . $redir);
    die();
} else {
    // Login failed, redirect back to login page to retry
    $getArgs = [];
    
    if (isset($_REQUEST['redir'])) {
        $getArgs['redir'] = $_REQUEST['redir'];
    }
    
    if (isset($_REQUEST['error'])) {
        $getArgs['error'] = $_REQUEST['error'];
    }
    
    header('Location: ./login.php' . empty($getArgs) ? '' : ('?' . http_build_query($getArgs)));
    die('Unauthorized');
}